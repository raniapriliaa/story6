from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *

# Create your tests here.
class Story9_Test(TestCase):
    def test_story9_is_exist(self):
      response = Client().get('/')
      self.assertEqual(response.status_code, 200)

    def test_story9_404_error(self):
      response = Client().get('/home/')
      self.assertEqual(response.status_code, 404)
    
    def test_register_user_exists(self):
        response = self.client.get('/accounts/regist/')

        self.assertContains(response, 'Username')
        self.assertContains(response, 'Password')

    def test_register_user(self):
        response = self.client.post('/accounts/regist/', follow=True, data={
            'username': 'rani',
            'password': '1234lala',
            'passwordconf': '1234lala',
        })

        self.assertEqual(response.status_code, 200)
  
    # def test_login_page(self):
    #     response = self.client.get('/user/login')

    #     self.assertContains(response, 'Masuk')
    
    def test_login_user(self):
        response = self.client.post('/accounts/login/', follow=True, data={
            'username': 'rani',
            'password': 'rani123',
        })

        self.assertEqual(response.status_code, 200)
        # self.assertContains(response, 'Selamat Datang', html=True)
    
    def test_logout_user(self):
        self.client.login(username='rani', password='1234lala')
        response = self.client.get('/accounts/logout/', follow=True)

        # self.assertRedirects(response, '/user/', status_code=200, target_status_code=200)
        self.assertContains(response, "Welcome to Rani's Site")

    

