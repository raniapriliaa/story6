from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Status
from . import forms

def create_status(request):
    
    if request.method == "POST":
        form = forms.StatusForm(request.POST or None)
        if form.is_valid():
            form.save()
            status = Status.objects.all().order_by('-id')
            response = {'status': status, 'title': 'List', 'form':form}
            return render(request, "landingpage.html", response)
    else:
        form = forms.StatusForm()
        status = Status.objects.all().order_by('-id')
        response = {'status': status, 'title': 'List', 'form':form}
    return render(request, "landingpage.html", response)

def story7(request):
    return render(request, "story7.html")

def books(request):
    return render(request,"books.html")



