from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from importlib import import_module
from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.conf import settings
from .views import *
import time
import unittest

from .views import create_status, story7
from .models import Status


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class TestLandingPage(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_404_error(self):
        response = Client().get('/wek/')
        self.assertEqual(response.status_code, 404)

    def test_status_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landingpage.html')
        response2 = Client().get('/story7/')
        self.assertTemplateUsed(response2, 'story7.html')

    def test_status_post_using_index_template(self):
        response = Client().post('/', {'status':"TEST"})
        self.assertTemplateUsed(response, 'landingpage.html')
    
    def test_profilepage_has_title(self):
        response = Client().get('/story7/')
        html_response = response.content.decode('utf8')
        self.assertIn("Rani Aprilia Astrianty", html_response)

    def test_profilepage_is_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_profilpage_has_name(self):
        response = Client().get('/story7/')
        html_response = response.content.decode('utf8')
        self.assertIn("Rani", html_response)
    
    def test_profilpage_has_photo(self):
        response = Client().get('/story7/')
        html_response = response.content.decode('utf8')
        self.assertIn('img', html_response)
    
    #Unit Test for Books page

    def test_bookspage_using_index_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')

    def test_bookspage_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_bookspage_has_title(self):
        response = Client().get('/books/')
        html_response = response.content.decode('utf8')
        self.assertIn("Books Library", html_response)
    
    def test_using_books(self):
        found = resolve('/books/')
        self.assertEqual(found.func, books)
    


    


class StatusFunctionalTest(LiveServerTestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser  = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(StatusFunctionalTest, self).setUp()
    def tearDown(self):
        self.browser.quit()
        super(StatusFunctionalTest, self).tearDown()
    def test_input_status(self):
        selenium = self.browser
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        # find the form element
        status = selenium.find_element_by_id('id_status')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        time.sleep(2)
        status.send_keys('Test')

        # submitting the form
        time.sleep(2)
        submit.send_keys(Keys.RETURN)

        time.sleep(2)
        assert 'Test' in selenium.page_source
