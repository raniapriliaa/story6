from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView
app_name = 'status'

urlpatterns = [
    path('', views.create_status, name='create_status'),
    path('story7/', views.story7, name='story7'),
    path('books/', views.books, name = 'books'),
    

]