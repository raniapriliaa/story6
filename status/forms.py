from django import forms
from .models import Status

class StatusForm(forms.ModelForm):

    status = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "form-control",
        "placeholder" : "Tell me how you feel!",
    }))

    class Meta:
        model = Status
        fields = ['status']