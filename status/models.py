from django.db import models
from datetime import datetime

class Status(models.Model):
    status = models.CharField(max_length = 300)
    updated_at = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
    

