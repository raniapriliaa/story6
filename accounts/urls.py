from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView
app_name = 'accounts'

urlpatterns = [
    
    path('home/', views.indexView, name = 'home'),
    path('dashboard/', views.dashboardView, name = 'dashboard'),
    path('login/',LoginView.as_view(), name='login_url'),
    path('regist/', views.regist, name='regist'),
    path('logout/',LogoutView.as_view(next_page ='accounts:home' ), name='logout'),

]